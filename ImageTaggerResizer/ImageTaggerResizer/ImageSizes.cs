using System;

namespace ImageHandler
{
    public class ImageSizes
    {
        private string _Description;
        private float _Width;
        private float _Height;

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }
        public float Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        public float Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
            }
        }

        private ImageSizes()
        { }

        public ImageSizes(String Description, int Width, int Height)
        {
            this.Description = Description;
            this.Width = (float)Width;
            this.Height = (float)Height;
        }

        public ImageSizes(String Description, float Width, float Height)
        {
            this.Description = Description;
            this.Width = Width;
            this.Height = Height;
        }
    }
}
