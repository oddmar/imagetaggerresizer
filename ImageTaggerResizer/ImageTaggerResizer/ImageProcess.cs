using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace ImageHandler
{
    public class ImageProcess
    {
        public string SaveToPath { get; set; }
        public string ImageText { get; set; }
        public Boolean OverwriteOriginal { get; set; }
        public int Quality { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public delegate void ImageProcessedHandler(object sender);

        public event ImageProcessedHandler ImageProcessed;

        public ImageProcess()
        {
            SaveToPath = "";
            ImageText = "";
        }

        public String FileName { get; private set; }

        public ListViewItem listViewItem;

        public void ProcessImage()
        {
            Image image;

            FileInfo fi = new FileInfo(this.listViewItem.Tag.ToString());

            if (!File.Exists(fi.FullName))
                return;
            FileName = fi.Name;

            if (Width == 0 && Height == 0)
                image = Image.FromFile(fi.FullName);
            else
                image = ResizeImage(fi.FullName, (int)Width, (int)Height, true);
            //image = GetThumbnail(fi.FullName, new ImageSizes("", this.Height, this.Width));

            using (Bitmap bm = new Bitmap(image))
            {
                using (Graphics g = Graphics.FromImage(bm))
                {
                    int fontMinimumSize = 10;
                    float fontSize = (float)Math.Round((Double)image.Height / 50, 0);
                    fontSize = fontSize <= fontMinimumSize ? fontMinimumSize : fontSize;
                    using (Font f = new Font("Arial", fontSize, GraphicsUnit.Pixel))
                    {
                        using (Brush whiteBrush = new SolidBrush(Color.White))
                        {
                            using (Brush blackBrush = new SolidBrush(Color.Black))
                            {
                                using (StringFormat sf = new StringFormat())
                                {
                                    sf.Alignment = StringAlignment.Far;
                                    sf.LineAlignment = StringAlignment.Far;
                                    g.DrawString(ImageText, f, blackBrush, image.Width - 2, image.Height - 2, sf);
                                    g.DrawString(ImageText, f, blackBrush, image.Width - 2, image.Height, sf);
                                    g.DrawString(ImageText, f, blackBrush, image.Width, image.Height - 2, sf);
                                    g.DrawString(ImageText, f, blackBrush, image.Width, image.Height, sf);
                                    g.DrawString(ImageText, f, whiteBrush, image.Width - 1, image.Height - 1, sf);
                                }
                            }
                        }
                    }
                    String saveFileName;
                    saveFileName = SaveToPath + "\\" + fi.Name;
                    bm.Save(saveFileName, ImageFormat.Jpeg);
                }
            }
            image.Dispose();

            if (ImageProcessed != null)
                ImageProcessed(this);
        }

        private Image ResizeImage(string OriginalFile, int Width, int Height, bool OnlyResizeIfWider)
        {
            Image FullsizeImage = Image.FromFile(OriginalFile);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= Width)
                {
                    Width = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * Width / FullsizeImage.Width;
            if (NewHeight > Height)
            {
                // Resize with height instead
                Width = FullsizeImage.Width * Height / FullsizeImage.Height;
                NewHeight = Height;
            }

            Image newImage = FullsizeImage.GetThumbnailImage(Width, NewHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

            // Save resized picture
            return newImage;
        }

        Image GetThumbnail(String file, ImageSizes imageSize)
        {
            Image image = Image.FromFile(file);
            float thumbWidth, thumbHeight;
            using (image)
            {
                float AspectRatio = (float)image.Width / (float)image.Height;

                if (image.Height >= image.Width)
                {
                    thumbHeight = imageSize.Height;
                    thumbWidth = (int)((float)imageSize.Height * AspectRatio);
                }
                else
                {
                    thumbHeight = (int)((float)imageSize.Height / AspectRatio);
                    thumbWidth = imageSize.Height;
                }

                int w = (int)thumbWidth;
                int h = (int)thumbHeight;

                Image thumbnailImage = image.GetThumbnailImage(w, h, new Image.GetThumbnailImageAbort(ThumbnailCallback_static), IntPtr.Zero);

                return thumbnailImage;
            }
        }

        public static bool ThumbnailCallback_static()
        {
            return true;
        }
    }
}
